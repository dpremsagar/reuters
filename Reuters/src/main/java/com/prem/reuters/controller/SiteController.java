package com.prem.reuters.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.prem.reuters.model.Road;
import com.prem.reuters.model.Route;
import com.prem.reuters.model.Site;
import com.prem.reuters.model.SiteDetails;
import com.prem.reuters.service.SiteService;


@RestController
public class SiteController {
	@Autowired
	private SiteService service;
	
	public SiteService getService() {
		return service;
	}
	public void setService(SiteService service) {
		this.service = service;
	}
	@RequestMapping(value = "/sites", method = RequestMethod.GET)
	public ResponseEntity<List<Site>> getAllSiteNames() throws Exception{
		List<Site> siteList = service.getAllSites();
		HttpStatus status = HttpStatus.OK;
		ResponseEntity<List<Site>> response = new ResponseEntity<>(siteList, status);
		return response;
	}

    @RequestMapping(value = "/sites/{siteName}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<SiteDetails> getSiteDetails(@PathVariable String siteName)  {
    	HttpStatus status = null;
    	ResponseEntity<SiteDetails> response = null;
    	try{
	    SiteDetails sd = service.getSiteDetailsByName(siteName);
	    status = (sd==null)?HttpStatus.NO_CONTENT:HttpStatus.OK;
	    response = new ResponseEntity<>(sd,status);
    	}catch(Exception e){
    		e.printStackTrace();
    		response = new ResponseEntity<>(new SiteDetails(),HttpStatus.NOT_FOUND);
    	}
        return response;
    }
    
    //returning boolean is not a good choice
    @RequestMapping(value = "/sites/{siteName}", method = RequestMethod.DELETE)
    public boolean deleteSiteByName(@PathVariable String siteName) throws Exception {
        return service.deleteSite(siteName);
    }

	@RequestMapping(value = "/sites", method = RequestMethod.POST)
	public ResponseEntity<Site> addSite(@RequestBody SiteDetails sd) throws Exception{
		Site site = service.addSite(sd);
		return new ResponseEntity<Site>(site,HttpStatus.CREATED);
	}

    @RequestMapping(value = "/sites/{siteName}", method = RequestMethod.PUT)
    public boolean updateSiteByName(@PathVariable String siteName,@RequestBody SiteDetails sd) throws Exception {
        service.updateSite(siteName,sd);
        return true;
    }
    
	@RequestMapping(value = "sites/{siteName}/routes", method = RequestMethod.POST)
	public boolean addRoutesToSite(@PathVariable String siteName, @RequestBody List<Road> connectionList) throws Exception{
		service.addRoutesToSite(siteName,connectionList);
		return true;
	}

    @RequestMapping(value = "/sites/{siteName}/routes/{destination}", method = RequestMethod.DELETE)
    public boolean deleteRoute(@PathVariable String siteName, @PathVariable String destination) throws Exception {
        return service.deleteRoute(siteName,destination);
    }

    @RequestMapping(value = "/sites/{siteName}/routes/{destination}", method = RequestMethod.PUT)
    public boolean updateRoute(@PathVariable String siteName, @PathVariable String destination, @RequestBody Road road) throws Exception {
        return service.updateRoute(siteName,destination,road);
    }


}
