package com.prem.reuters.model;

import java.util.ArrayList;
import java.util.List;

public class SiteDetails {
	private String site;
	private List<Road> connectionList = new ArrayList<>();
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public List<Road> getConnectionList() {
		return connectionList;
	}
	public void setConnectionList(List<Road> connectionList) {
		this.connectionList = connectionList;
	}
	public void addConnection(Road road){
		this.connectionList.add(road);
	}
}
