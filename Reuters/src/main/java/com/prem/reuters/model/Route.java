package com.prem.reuters.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class Route {

	private Long id;
	private int length;
	//@JsonBackReference
	List<Site> endpoints = new ArrayList<>();
	public Route(){
		
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public List<Site> getEndpoints() {
		return endpoints;
	}
	public boolean addEndpoints(Site site) {
		if(this.endpoints.size()>=2){
			return false;
		}
		this.endpoints.add(site);
		return true;
	}

}
