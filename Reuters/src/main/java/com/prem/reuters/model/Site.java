package com.prem.reuters.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"site","link"})
public class Site {
	private Long id;
	@JsonProperty("site")
	private String name;
	@JsonBackReference
	private List<Route> routes = new ArrayList<>();
	private String link;
	public Site(){
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@JsonBackReference
	public List<Route> getRouteList() {
		return routes;
	}
	public void addRoute(Route route) {
		this.routes.add(route);
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}

	

}
