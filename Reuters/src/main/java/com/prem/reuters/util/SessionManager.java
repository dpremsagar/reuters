package com.prem.reuters.util;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.stereotype.Component;

@Component
public class SessionManager {
	private SessionFactory sf;
	public SessionManager() {
	     sf =  new SessionFactory("com.prem.reuters.model");
		 try {
			Class.forName("org.neo4j.ogm.drivers.http.driver.HttpDriver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public Session provideSession(){
		return sf.openSession();
	}

}
