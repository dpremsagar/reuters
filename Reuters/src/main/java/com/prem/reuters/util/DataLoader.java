package com.prem.reuters.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.prem.reuters.repository.SiteRepository;

public class DataLoader  {

	private static SiteRepository repo;
	
	public static void main(String[] args) throws Exception {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("/spring/dataloader-config.xml");
		repo = context.getBean(SiteRepository.class,"repo" );
		loadSiteData();
		loadRouteData();
		context.close();
	}
	private static void loadRouteData() throws NumberFormatException, Exception {
			
			try
			(BufferedReader fileReader = 
			new BufferedReader(new FileReader ("/Users/Prem/Documents/Resumes/Thomson-Reuters-Sunnyvale/JavaCodingTestData-distances.csv")))
			{	
				String line = "";
				while((line = fileReader.readLine())!=null){
					String[] tokens = line.split(",");
					repo.addRoute((int)Math.round((Double.parseDouble(tokens[1]))), tokens[0].toLowerCase(), tokens[2].toLowerCase());
				}
			}  catch (IOException e) {
				e.printStackTrace();
			}

	}
	private static void loadSiteData() throws Exception {
		try {
			Scanner scanner = new Scanner( new File ("/Users/Prem/Documents/Resumes/Thomson-Reuters-Sunnyvale/JavaCodingTestData-sites.csv"));
			scanner.useDelimiter(",");
			while(scanner.hasNextLine()){
				String siteName = scanner.nextLine().toLowerCase();
				repo.addSitebyName(siteName);
				
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}


	
}
