package com.prem.reuters.repository;

import java.util.List;

import com.prem.reuters.model.Site;

public interface SiteRepository {

	Site getSiteDetailsByName(String name) throws Exception;
	List<Site> getAllSiteNames() throws Exception;
	boolean deleteSitebyName(String name) throws Exception;
	boolean addSitebyName(String name) throws Exception;
	boolean addRoute(int length,String endPoint1, String endPoint2) throws Exception;
	boolean updateSite(String currentName, String newName) throws Exception;
	boolean deleteRoute(String endPoint1, String endPoint2) throws Exception;
	boolean updateRoute(String endPoint1, String endPoint2, int length) throws Exception;
}
