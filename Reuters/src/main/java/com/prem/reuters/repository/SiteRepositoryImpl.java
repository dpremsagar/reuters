package com.prem.reuters.repository;

//import static com.prem.reuters.util.SessionManager.provideSession;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.prem.reuters.model.Route;
import com.prem.reuters.model.Site;
import com.prem.reuters.util.SessionManager;

@Repository
public class SiteRepositoryImpl implements SiteRepository {
	
	private Session session;
	@Autowired
	private SessionManager sessionManager;
	

	@Override
	public Site getSiteDetailsByName(String name){
		session = sessionManager.provideSession();
		Filter filter = new Filter("name",name);
		Collection<Site> sites = session.loadAll(Site.class,filter,2);//setting depth to 2
		if(sites.isEmpty()){return null;}
		Optional<Site> findFirst = sites.stream().findFirst();
		return findFirst.get();
	}

	@Override
	public List<Site> getAllSiteNames() {
		session = sessionManager.provideSession();
		//Second argument depth is set to 0 so as not to obtain any neighbor node info
		Collection<Site> siteColl = session.loadAll(Site.class, 0);
		List<Site> siteList = new ArrayList<>();
		siteList.addAll(siteColl);
		return siteList;
	}

	@Override
	public boolean deleteSitebyName(String name) {
		session = sessionManager.provideSession();
		Filter filter = new Filter("name",name);
		Collection<Site> sites = session.loadAll(Site.class,filter);
		sites.stream().forEach((endPoint)->session.delete(endPoint.getRouteList()));
		session.delete(sites);
		return true;
	}

	@Override
	public boolean addSitebyName(String name) {
	    session = sessionManager.provideSession();
	    Filter filter = new Filter("name",name);
	    Collection<Site> sitesRetrieved = session.loadAll(Site.class, filter);
	    if(!sitesRetrieved.isEmpty()){
	    	System.out.println("Site exists");
	    	return false;
	    }
		Site site = new Site(); 
		site.setName(name);
		session.save(site);
		System.out.println("Adding site to db");
		return true;
	}
	public boolean addRoute(int length,String endPoint1, String endPoint2) {
		session = sessionManager.provideSession();
		Route route = new Route();
		route.setLength(length);
		Site ep1 = getSiteDetailsByName(endPoint1);
		Site ep2 = getSiteDetailsByName(endPoint2);
		route.addEndpoints(ep1);
		ep1.addRoute(route);
		ep2.addRoute(route);
		route.addEndpoints(ep2);
		session.save(route);
		session.save(ep1);session.save(ep2);
		return true;
	}
	public boolean updateSite(String currentName, String newName) {
		Filter filter = new Filter("name",currentName);
		System.out.println(currentName + newName);
		Collection<Site> sites = session.loadAll(Site.class,filter);
		sites.stream().forEach((site)->{
			site.setName(newName);
			session.save(site);			
		});
/*		for(Site site : sites){
			site.setName(newName);
			session.save(site);
		}
*/		return true;
	}
	/*
	 * Load the endPoint1 site and its routes first
	 * Find the matching route and delete
	 */
	@Override
	public boolean deleteRoute(String endPoint1, String endPoint2) {
		session = sessionManager.provideSession();
		Filter filter = new Filter("name",endPoint1);
		Collection<Site> sites = session.loadAll(Site.class,filter,2);//setting depth to 2
		if(sites.isEmpty()){return true;}
		Optional<Site> findFirst = sites.stream().findFirst();
		Site site = findFirst.get();
		site.getRouteList().stream().forEach((route)->{
			List<Site> endpoints = route.getEndpoints();
			for(Site city : endpoints){
				if(city.getName().equals(endPoint2))
					session.delete(route);
			}	
		});
		return true;
	}

	@Override
	public boolean updateRoute(String endPoint1, String endPoint2, int length) {
		session = sessionManager.provideSession();
		Filter filter = new Filter("name",endPoint1);
		Collection<Site> sites = session.loadAll(Site.class,filter,2);//setting depth to 2
		if(sites.isEmpty()){return true;}
		Optional<Site> findFirst = sites.stream().findFirst();
		Site site = findFirst.get();
		site.getRouteList().stream().forEach((route)->{
			List<Site> endpoints = route.getEndpoints();
			for(Site city : endpoints){
				if(city.getName().equals(endPoint2))
					route.setLength(length);
					session.save(route);
			}	
		});
		return true;
	}

	public SessionManager getSessionManager() {
		return sessionManager;
	}

	public void setSessionManager(SessionManager sessionManager) {
		System.out.println("setting session manager");
		this.sessionManager = sessionManager;
	}
}
