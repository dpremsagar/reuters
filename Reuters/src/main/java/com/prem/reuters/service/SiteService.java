package com.prem.reuters.service;

import java.util.List;

import com.prem.reuters.model.*;
public interface SiteService{

	boolean addSite(String name) throws Exception;
	List<Site> getAllSites() throws Exception;
	SiteDetails getSiteDetailsByName(String siteName) throws Exception;
	boolean deleteSite(String name) throws Exception;
	Site addSite(SiteDetails sd) throws Exception;
	void updateSite(String siteName, SiteDetails sd) throws Exception;
	boolean addRoutesToSite(String siteName,List<Road> connectionList) throws Exception;
	boolean deleteRoute(String siteName, String destination) throws Exception;
	boolean updateRoute(String siteName, String destination, Road road) throws Exception;
}
