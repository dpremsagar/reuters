package com.prem.reuters.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prem.reuters.model.Road;
import com.prem.reuters.model.Route;
import com.prem.reuters.model.Site;
import com.prem.reuters.model.SiteDetails;
import com.prem.reuters.repository.SiteRepository;
import com.prem.reuters.repository.SiteRepositoryImpl;


@Service
public class SiteServiceImpl implements SiteService {
	@Autowired
	private SiteRepository repo;
	
	private static final String BASEURL = "http://localhost:8181/Reuters/";
	private static final String ROUTES = "routes/";
	private static final String SITES = "sites/";
	
	public SiteRepository getRepo() {
		return repo;
	}

	public void setRepo(SiteRepository repo) {
		this.repo = repo;
	}

	@Override
	public boolean addSite(String name) throws Exception {
		repo.addSitebyName(name);
		return true;
	}
	
	@Override
	public List<Site> getAllSites() throws Exception {
		List<Site> siteList = repo.getAllSiteNames();
		siteList.stream().forEach((site)->site.setLink(BASEURL+SITES+site.getName()));
		return siteList;
	}	

	@Override
	public SiteDetails getSiteDetailsByName(String siteName)throws Exception {
		SiteDetails sd = new SiteDetails();
		Site site = repo.getSiteDetailsByName(siteName);
		sd.setSite(site.getName());
		List<Route> routeList = site.getRouteList();
		for(Route route : routeList){
			Road road = new Road();
			road.setDistance(route.getLength());
			List<Site> endpoints = route.getEndpoints();
			for(Site city : endpoints){
				String cityName = city.getName();
				if( cityName != sd.getSite() ){
					road.setDestination(cityName);
				    road.setLink(BASEURL+SITES+cityName);
				}
			}
			sd.addConnection(road);
		}
		return sd;
	}

	@Override
	public boolean deleteSite(String name) throws Exception {
		repo.deleteSitebyName(name);
		return true;
	}

	@Override
	@Transactional
	public Site addSite(SiteDetails sd)throws Exception {
		if(sd.getConnectionList().isEmpty()){
			repo.addSitebyName(sd.getSite());
		}
		else{
			repo.addSitebyName(sd.getSite());
			List<Road> connList = sd.getConnectionList();
			for(Road road : connList){
				repo.addRoute(road.getDistance(), sd.getSite(), road.getDestination());
			}
		}
		Site site = new Site();
		site.setName(sd.getSite());
		site.setLink(BASEURL+SITES+sd.getSite());
		return site;
	}

	@Override
	public void updateSite(String siteName, SiteDetails sd)throws Exception {
		if(sd.getConnectionList().isEmpty()){
			repo.updateSite(siteName, sd.getSite());
		}
		
	}

	@Override
	@Transactional
	public boolean addRoutesToSite(String siteName, List<Road> connectionList)throws Exception {
		connectionList.stream().forEach((road)->{
			try{
				repo.addRoute(road.getDistance(), road.getDestination(), siteName) ;
			}catch(Exception e){
				throw new RuntimeException(e);
			}
			});
		return true;
	}

	@Override
	public boolean deleteRoute(String siteName, String destination) throws Exception{
		repo.deleteRoute(siteName, destination);
		return true;
	}

	@Override
	public boolean updateRoute(String siteName, String destination, Road road) throws Exception{
		repo.updateRoute(siteName, destination, road.getDistance());
		return false;
	}

}
